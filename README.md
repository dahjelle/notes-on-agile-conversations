# Notes on _Agile Conversations_

[_Agile Conversations_ by Douglas Squirrel and Jeffrey Fredrick](https://agileconversations.com/agile-conversation-book/) _sounds_ like it is a book having conversations about agile software development methodologies. While its context is software development, and it uses examples from that field, it really has advice for **everyone having conversations and needing to communicate effectively in person.**

They outline 5 types of conversations that they would say all teams need to work together effectively. They also give many tools for _improving_ and _practicing_ your own conversations. Unlike their book, I'll outline the conversations first, then discuss some of the tools.

One main thing to keep in mind: **people are not machines**. We all work in different ways, and do not always have the same output to the same input. The perfect process won't always work, and you will need to regularly adapt to the situation at hand.

>   I think these ideas are useful in a lot of scenarios, including:
>
>   -   company meetings
>   -   problem solving with another developer
>   -   working through an issue with a customer
>   -   understanding the needs of a prospective customer
>   -   finding resolution when disagreeing with a teammate
>   -   working together more effectively

[[_TOC_]]

## 1) The Trust Conversation

>   If I say I trust you, I mean that I have expectations about what you will do that have been met before and that I believe will be met again. I can use the story we agree on to predict your behavior and evaluate my possible actions, so that we can cooperate effectively.

It is **typical** for two people to have inner stories about a situation that explain the situation, but _are **different** from each other_ — which makes it very difficult to work together. They keep doing things the other person is surprised by. How do you solve this?

-   be vulnerable
    -   you may have to admit you don't know or ask dumb-sounding questions
    -   be ready to learn something new from the conversation
-   don't be a hypocrite
    -   make sure your story that you share _matches_ your actions
    -   if it hasn't in the past, you may have a lot of work to rebuild trust
-   "test-driven development for people"
    -   allows two people to align stories by building step-by-step — check to see at which level your stories diverge
        -   start with _observable data and experiences_
        -   look at how you might _filter_ that data
        -   look at how you add _cultural and personal meaning_ to that data
        -   look at _assumptions_ you make based off of those meanings
        -   look at the _conclusions_ you draw
        -   look at the new _beliefs_ you adopt
        -   look at the _actions_ you take based off of those beliefs
    -   note that only outside data and experiences (the first step) and actions you take (the last step) are observable _outside your head_ — it takes a conversation to share or uncover all the links between those two steps
-   be ready to change your story
    -   you will likely find out that the reason the other person does or says X is far different than you thought

## 2) The Fear Conversation

> Fear paralyzes a team, inhibiting creativity and cooperation…[Instead,] create psychological safety and courage in your team by revealing fears and making it okay to mitigate them.
>
> …The Fear Conversation revealed that developers believed the would be reprimanded or fired if their actions turned out badly, and there were valid reasons to believe this…it also turned out that the organization was [actually] very risk friendly.

Human's natural reaction to an uncertain situation _tends_ to be fear of the unknown rather than curiosity. This serves us well in a dangerous world, but inhibits learning and discovery.  Why would you buck the status quo and improve something when you felt there was a serious risk involved in doing so? Further, fears are often hidden rather than discussed forthrightly. How does your team solve this?

- be curious — other's fears will surprise you
- be vulnerable — it can be very difficult to admit to our own fears
- watch out for "normalization of deviance"
    - this is when you _say_ you value one thing, but your actions prove otherwise
    - you say: "we value software with few bugs", but you ship without sufficient testing
- watch out for _assuming_ why others may think or act as they do
    - you may assume a person is rude because they don't approve of you
    - instead, _imagine possible alternative reasons_:
        - they use rudeness as a secret intra-office code
        - they had a poor night of sleep
        - they just received bad personal news
        - they *wouldn't* have found the same phrase rude themselves
    - it is helpful to use ridiculous explanations to help coax our brain out of its assumptions
- create a fear chart (with one or many others)
    - make a list on stickies or a whiteboard (including ridiculous ones!)
    - see if any can be combined or inspire new ideas
    - filter out fears that we are content to live with
    - figure out how to mitigate the fears, such as agreeing to get everyone's "thumbs-up" before a release to make sure everyone is on-board

## 3) The Why Conversation

> Building a Why gives our company a strategic direction that guides large and small decisions…[it] must not only explain the impetus for your collective action as a team but be created _jointly_, with all those involved. An executive who imposes a Why from above — or worse, with only a sham consultation — does more harm than good.
>
> Joint Design [of the Why] is the only way to create internal commitment and self-organization in a team.
>
> The key elements [of joint design] are _inclusion_ (I was a part of the decision, and my objections were heard.) and _information flow_ (I was able to share what I know.).

- many people start with _why_, but your team needs to have trust and reduce fear, first
- tools
    - interests, not positions — more room for finding common ground if you dig deeper and discuss interests (i.e. we need to keep up with competitors) than if you are discussing specific positions (i.e. we need to release feature X in two months)
    - combine advocacy and inquiry — both state our views and reasoning while also asking questions to understand others' positions (you don't have sole access to the truth)
- process of joint design
    - include as many people as possible
    - ask genuine questions
    - invite opposing views
    - set time limits for the discussion
    - establish and communicate who will make the final decision

## 4) The Commitment Conversation

> Compliance is doing what you are told…[it] fails exactly when…the team needs to identify and overcome unknown obstacles…by taking on a new challenge.
>
> Compliance is showing up; commitment is engaging with your whole self. Compliance is filing the space; commitment is participating. Compliance can be enough for routine day-to-day tasks; compliance is not enough to generate change, to improve, to excel.

- _why_ people commit is highly individual and personal (pay, mastery, hours worked, etc.)
- you **won't** get commitment if you team has low trust, unmitigated fear, or if they were left out of designing the "why"
- requires _clarity_ on what you are committing to
    - what does "done" mean (i.e. reading for testing? ready for a beta user? ready for 1000s of users?)
    - _very_ common to have the same words mean different things to different people and circumstances — have a conversation for alignment!
- easiest if you make each individual commitment as small as possible
    - a "walking skeleton" is the smallest possible step that does _something_ useful, and then iteratively improve _without breaking_ the main functionality
- a successful commitment conversation is:
    1. agree on the _meaning_ of the commitment
    2. agree on the _next outcome_ of the commitment
    3. reaffirm the commitment
- are people willing to make commitments and stick to them?

> "Gee, everyone, there's nothing to be afraid of here. We understand the tasks, and we know each one is individually simple and achievable. If we can't complete this set of tasks by that date, in fact much sooner, we should go into another line of work."

## 5) The Accountability Conversation

> If we are being repeatedly…surprised…the normal approach is to ask for more detailed information [and] to provide more specific instructions…[T]hey miss the root of the problem: accountability.
>
> [Accountability is] being obligated to render an account of what you have done and why. Accountability is akin to ownership, to responsibility, and to agency.
>
> Each team member is empowered to make their own decisions about how to allocate time and energy. However, with this empowerment, comes an expectation to share what those decisions are and why they were made.

- three gaps in teamwork
    - _knowledge gap_ between what we would like to know and what we actually know
    - _alignment gap_ between what we want people to do and what they actually do
    - _effects gap_ between what we expect our actions to achieve and what they actually do
- use a "briefing" to give direction (i.e. "where we are going"), and use a "back briefing" to confirm understanding (i.e. "how we plan to get there")
    - _briefing_ includes:
        - desired outcome
        - freedoms and constraints
        - _not_ specific ways to accomplish the goal
        - realizes that managers don't have information about the minutiae of the work
    - _back briefing_ includes:
        - how it plans to achieve the desired outcome
        - confirmation that the plan meets the outcome, freedoms, and constraints
- you should be "radiating intent" at all times — others knowing our intended direction helps them provide useful information and adjust their plans
    - what is the current state?
    - what are your plans and intended outcomes?
    - what obstacles are possible?
- the initial conversation is easy; much harder to **keep** having the conversation

> [On the Boeing 777, an] engineer could spend up to \$300 to save a pound [of airplane weight] without approval; spending up to \$600 per pound needed only a local supervisor's okay; and even more, up to \$2,500, could be spent to save a pound if a program manager gave the go-ahead.

## Tools for Better Conversations

It's nice to know about all these conversations, but how do you get _better_ at them? As usual: practice. What is not usual (for most people) is practicing conversations, and _Agile Conversations_ has several techniques that can help.

### 1) be aware of cognitive biases

**Your own** behaviors are contributing to the problem!

- the **egocentric bias** gives self undue credit for positive outcomes
- the **false consensus effect** believes that one's personal views are more common than in reality
- the **gambler's fallacy** believes that a random event is influenced by previous outcomes (especially prevalent in "this is why I was successful" stories)
- the **illusion of control** overestimates control over external events
- the **loss aversion** bias values keeping something rather than getting something better
- the **naïve realism** bias assumes that one's one view doesn't have any bias
- the **negativity bias** recalls bad things more than good things
- the **normalcy bias** refuses to plan for unexpected things
- the **outcome bias** ignores the quality of a decision-making process in favor of only looking for good results

### 2) focus on learning & curiosity

> If my theory of action has a focus on learning, then I will take actions that generate information, like sharing everything I know that is relevant to the situation and asking others about what they know. If my theory of action is centered on getting my own way, then I will only share information that supports my position, and I won't ask questions to which I don't know the answer.

It takes a definite _humility_ to admit that you have more to learn and that you _might be wrong__.

### 3) defensive vs. productive reasoning

When making an important decision as a group, most people _say_ they would get all the information they can and then seek consensus. They likely do this in less important decisions. What most people **actually do** in important decisions is make a unilateral decision, sharing only the information that supports their view — their reasoning has become **defensive** rather than productive.

Humans are diverse; **productive conflict** is where the best decisions and ideas come from.

### 4) Record, Reflect, Revise, and Role Play (4 R's) to practice conversations

You can have better conversations with practice.

- **record** the conversation *on paper*
    - real or hypothetical conversation, spoken or written (chat)
    - two columns
        - right column: what each person said, close to a tape recorder
        - left column: what _you_ thought at the time, but not the other person (you don't know)
    - score the conversation
        - **curiosity**: how many questions where genuine (i.e. want to know the answer and are willing to change based on the answer, not leading); higher is better
        - **transparency**: underline all thoughts & feelings of yours that were not even partially expressed in the conversation
        - **patterns**: what triggers you to react strongly? what signals do you give that you are about to not be curious and transparent? what instinctive responses do you have?
- **reflect** on how you could have done better
- **revise** the conversation with what you learned
- **role play** to see how it works in practice (real conversations are often different than paper, but a real person on the other end — even if they are only playing a part! — can help)